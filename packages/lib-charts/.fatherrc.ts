import {resolve} from 'path'
import {IBundleOptions} from 'father'

interface IOptions extends Omit<IBundleOptions, 'doc'> {
  doc?: Partial<IBundleOptions['doc']>;
}

const options: IOptions = {
  cjs: 'rollup',
  esm: 'rollup',
  umd: {
    minFile: true,
    file: 'charts',
    globals: {
      react: 'react',
      echarts: 'echarts',
    },
  },
  doc: {
    typescript: true,
    onCreateWebpackChain<C>(config: C): void {
      // @ts-ignore
      config.resolve.alias
        .set('@', resolve(__dirname, './src/'))
    },
  },
}

export default options
