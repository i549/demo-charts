import React, {useEffect} from 'react'

const draw = (canvas: HTMLCanvasElement, range: HTMLInputElement) => {
  const ctx = canvas.getContext('2d')
  if(!ctx) {
    return
  }
  //range控件信息
  let rangeValue: number = parseFloat(range.value);
  let nowRange: number = 2;   //用于做一个临时的range

//画布属性
  let mW = canvas.width = 350;
  let mH = canvas.height = 350;
  let lineWidth = 1;

//圆属性
  let r = mH / 2; //圆心
  let cR = r - 32 * lineWidth; //圆半径

//Sin 曲线属性
  let sX = 0;
  let axisLength = mW; //轴长
  let waveWidth = 0.012 ;   //波浪宽度,数越小越宽
  let waveHeight = 26; //波浪高度,数越大越高
  let speed = 0.048; //波浪速度，数越大速度越快
  let xOffset = 0; //波浪x偏移量

  ctx.lineWidth = lineWidth;

//画圈函数
  let IsdrawCircled = false;
  let drawCircle = function(){
    ctx.beginPath();
    ctx.strokeStyle = '#1080d0';
    ctx.arc(r, r, cR+1, 0, 2 * Math.PI);
    ctx.stroke();
    ctx.beginPath();
    ctx.arc(r, r, cR, 0, 2 * Math.PI);
    ctx.clip();
    IsdrawCircled = true;
  }

  //画sin 曲线函数
  let drawSin = function(xOffset: number, color: string | CanvasGradient, waveHeight: number){
    ctx.save();

    let points=[];  //用于存放绘制Sin曲线的点

    ctx.beginPath();
    //在整个轴长上取点
    for(let x = sX; x < sX + axisLength; x += 20 / axisLength){
      //此处坐标(x,y)的取点，依靠公式 “振幅高*sin(x*振幅宽 + 振幅偏移量)”
      let y = Math.sin((-sX - x) * waveWidth + xOffset) * 0.8 + 0.1;

      let dY = mH * (1 - nowRange / 100 );

      points.push([x, dY + y * waveHeight]);
      ctx.lineTo(x, dY + y * waveHeight);
    }

    //封闭路径
    ctx.lineTo(axisLength, mH);
    ctx.lineTo(sX, mH);
    ctx.lineTo(points[0][0],points[0][1]);
    ctx.fillStyle = color;
    ctx.fill();

    ctx.restore();
  };

  let render = function(){
    ctx.clearRect(0, 0, mW, mH);

    rangeValue = parseFloat(range.value);

    if(!IsdrawCircled){
      drawCircle();
    }

    if(nowRange <= rangeValue){
      let tmp = 1;
      nowRange += tmp;
    }

    if(nowRange > rangeValue){
      let tmp = 1;
      nowRange -= tmp;
    }

    const linearGradient = ctx.createLinearGradient(0,0,300,300);
    linearGradient.addColorStop(0, 'rgba(28, 134, 209, 0.8)')
    linearGradient.addColorStop(1, 'rgba(28, 128, 200, 0.2)')

    const linearGradient2 = ctx.createLinearGradient(0,0,200,200);
    linearGradient2.addColorStop(0, 'rgba(28,134,209,1)')
    linearGradient2.addColorStop(1, 'rgba(28,167,210,1)')

    drawSin(xOffset+Math.PI*0.7, linearGradient, waveHeight);
    drawSin(xOffset, linearGradient2, waveHeight);
    drawText();

    xOffset += speed;
    requestAnimationFrame(render);
  }
//写百分比文本函数
  let drawText = function(){
    ctx.save();

    let size = 0.4*cR;
    ctx.font = size + 'px Microsoft Yahei';
    ctx.textAlign = 'center';
    ctx.fillStyle = "rgba(06, 85, 128, 0.5)";
    ctx.fillText(~~nowRange + '%', r, r + size / 2);

    ctx.restore();
  };

  render();
}

const Wave: React.FC = props => {
  const radius = 200
  const ref = React.createRef<HTMLCanvasElement>()
  const inputRef = React.createRef<HTMLInputElement>()

  const drawCanvas = () => {
    if(ref.current && inputRef.current) {
      draw(ref.current, inputRef.current)
    }
  }

  useEffect(() => {
    drawCanvas()
  }, [])

  return (
    <div>
      <input type="range" id="r" min="0" max="100" step="1" ref={inputRef} onChange={() => {
        drawCanvas()
      }}/>
      <canvas
        width={radius * 2}
        height={radius * 2}
        ref={ref}
      >
        {'Your browser does not support canvas, please change it.'}
      </canvas>
    </div>
  )
}

export default Wave