import React, {CSSProperties, useEffect, useState} from 'react'

export interface WaveBallRenderParams {
  /** 边框宽度，默认 1 */
  borderWidth?: number;
  /** 数值(0 - 100)，默认 0 */
  value?: number;
}

export interface WaveBallActionType {
  refresh: (params: WaveBallRenderParams) => void;
}

export interface WaveBallProps extends WaveBallRenderParams {
  /** 半径，默认 200 */
  radius?: number;
  style?: CSSProperties;
  actionRef?: React.MutableRefObject<WaveBallActionType>
    | ((action: WaveBallActionType) => void);
}

const defaultRadius = 200

const drawCircle = (ctx: CanvasRenderingContext2D, r: number, cR: number) => {
  ctx.beginPath()
  ctx.strokeStyle = '#1080d0'
  ctx.arc(r, r, cR + 1, 0, 2 * Math.PI)
  ctx.stroke()
  ctx.beginPath()
  ctx.arc(r, r, cR, 0, 2 * Math.PI)
  ctx.clip()
}

//画sin 曲线函数
const drawSin = (
  ctx: CanvasRenderingContext2D,
  xOffset: number,
  color: string,
  waveHeight: number,
  p: any,
) => {
  ctx.save()
  const points = []  //用于存放绘制Sin曲线的点

  ctx.beginPath()
  //在整个轴长上取点
  for(let x = p.sX; x < p.sX + p.axisLength; x += 20 / p.axisLength){
    //此处坐标(x,y)的取点，依靠公式 “振幅高*sin(x*振幅宽 + 振幅偏移量)”
    const y = Math.sin((-p.sX - x) * p.waveWidth + xOffset) * 0.8 + 0.1
    const dY = p.mH * (1 - p.value / 100 )
    points.push([x, dY + y * waveHeight])
    ctx.lineTo(x, dY + y * waveHeight)
  }

  //封闭路径
  ctx.lineTo(p.axisLength, p.mH)
  ctx.lineTo(p.sX, p.mH)
  ctx.lineTo(points[0][0], points[0][1])
  ctx.fillStyle = color
  ctx.fill()

  ctx.restore()
}

const drawText = (
  ctx: CanvasRenderingContext2D,
  p: any,
) => {
  ctx.save()

  const size = 0.4 * p.cR
  ctx.font = size + 'px Microsoft Yahei'
  ctx.textAlign = 'center'
  ctx.fillStyle = 'rgba(06, 85, 128, 0.5)'
  ctx.fillText(~~p.range + '%', p.r, p.r + size / 2)

  ctx.restore()
}

const render = (
  ctx: CanvasRenderingContext2D,
  p: any,
) => {
  const animationRender = () => {
    console.log('render: ', p.value, p.range)
    ctx.clearRect(0, 0, p.mW, p.mH)

    if(!p.isDrawCircled) {
      drawCircle(ctx, p.r, p.cR)
      p.isDrawCircled = true
    }

    if(p.range <= p.value) {
      p.range += 1
    } else {
      p.range -= 1
    }

    drawSin(ctx, p.xOffset + Math.PI * 0.7, 'rgba(28, 134, 209, 0.5)', 18, p)
    drawSin(ctx, p.xOffset, '#1c86d1', 18, p)
    drawText(ctx, p)

    p.xOffset += p.speed

    return requestAnimationFrame(animationRender)
  }

  return animationRender()
}

const renderCanvas = (
  ctx: CanvasRenderingContext2D | null | undefined,
  params: WaveBallRenderParams,
) => {
  if(!ctx) {
    return
  }

  const canvas = ctx.canvas
  const {borderWidth = 1, value = 0} = params

  //圆属性
  const mW = canvas.width
  const mH = canvas.height
  const r = mW / 2
  const cR = r - 32 * borderWidth

  //Sin 曲线属性
  const sX = 0
  const axisLength = mW   //轴长
  const waveWidth = 0.008  //波浪宽度,数越小越宽
  const waveHeight = 8     //波浪高度,数越大越高

  //属性整合
  const p = {
    range: 0,
    //波浪速度,数越大速度越快
    speed: 0.08,
    //波浪x偏移量
    xOffset: 0,
    isDrawCircled: false,
    r, cR, sX, mH, mW, axisLength,
    waveHeight, waveWidth, value,
  }

  ctx.lineWidth = borderWidth
  return render(ctx, p)
}

const WaveBall: React.FC<WaveBallProps> = props => {
  const {children, radius = defaultRadius, style, actionRef, ...params} = props
  const [drawId, setDrawId] = useState<number>()
  const [waveParams, setWaveParams] = useState<WaveBallRenderParams>(params)
  const ref = React.createRef<HTMLCanvasElement>()

  const getContext = () => {
    if (ref.current) {
      return ref.current.getContext('2d')
    }
    return null
  }

  const draw = (params?: WaveBallRenderParams) => {
    const mergeParams = {...waveParams, ...params}
    drawId !== undefined && cancelAnimationFrame(drawId)
    const id = renderCanvas(getContext(), mergeParams)
    setDrawId(id)
    setWaveParams(mergeParams)
  }

  useEffect(() => {
    if (actionRef) {
      const action: WaveBallActionType = {
        refresh: (params) => {
          draw(params)
        }
      }
      if(typeof actionRef === 'function') {
        actionRef(action)
      } else {
        actionRef.current = action
      }
    }

    if(ref.current) {
      draw()
    }
  }, [])

  return (
    <div style={style}>
      <input type="range" id="r" min="0" max="100" step="1" onChange={(e) => {
        draw({value: parseFloat(e.target.value)})
      }}/>
      <canvas
        width={radius * 2}
        height={radius * 2}
        ref={ref}
      >
        {children || 'Your browser does not support canvas, please change it.'}
      </canvas>
    </div>
  )
}

export default WaveBall