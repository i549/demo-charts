import {ChartOption} from '@/types'

/**
 * 合并Charts Option对象
 * @param target 目标Option
 * @param source 源Option
 * @param overwrite 属性已存在时是否覆写
 */
export default (
  target: ChartOption,
  source: ChartOption,
  overwrite?: boolean,
) => {
  return target
}