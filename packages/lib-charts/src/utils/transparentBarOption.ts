import {EChartOption} from '@/types'

const Transparent = 'rgba(0,0,0,0)'

/**
 * 获取辅助透明柱状图Option
 * @param option
 */
export default (option: EChartOption.SeriesBar): EChartOption.SeriesBar => {
  return {
    type: 'bar',
    itemStyle: {
      color: Transparent,
      barBorderColor: Transparent,
    },
    emphasis: {
      itemStyle: {
        color: Transparent,
        barBorderColor: Transparent,
      },
    },
    ...option,
  }
}