import { EChartOption } from '@/types';

/**
 * 柱状图阴影配置
 * @param data y轴数据，根据数据获取最大值为阴影柱的值
 * @param top  相比y轴最大值，距离顶部的差值
 */
export default (data: number[], top: number = 0): EChartOption.SeriesBar => {
  return {
    type: 'bar',
    itemStyle: {
      color: 'rgba(0,0,0,0.05)',
    },
    barGap: '-100%',
    barCategoryGap: '40%',
    data: new Array(data.length).fill(
      Math.max(...data) + top
    ),
    animation: false,
  }
}