export { default as mergeOption } from './mergeOption'
export { default as shadowBarOption } from './shadowBarOption'
export { default as transparentBarOption } from './transparentBarOption'