import { EChartOption } from 'echarts'

export { EChartOption }

export type AxisData = (string | number | EChartOption.BasicComponents.CartesianAxis.DataObject)
export type SeriesBarData = EChartOption.SeriesBar['data']

export interface ChartOption<TSeries = EChartOption.Series> extends EChartOption<TSeries> {

}