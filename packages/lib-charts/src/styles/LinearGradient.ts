import echarts from 'echarts'

export const BAR_COLOR: any = new echarts.graphic.LinearGradient(
  0, 0, 0, 1,
  [
    {offset: 0, color: '#83bff6'},
    {offset: 0.5, color: '#188df0'},
    {offset: 1, color: '#188df0'},
  ],
)

export const BAR_EMPHASIS_COLOR: any = new echarts.graphic.LinearGradient(
  0, 0, 0, 1,
  [
    {offset: 0, color: '#2378f7'},
    {offset: 0.7, color: '#2378f7'},
    {offset: 1, color: '#83bff6'},
  ],
)

export const LINE_COLOR: any = new echarts.graphic.LinearGradient(0, 0, 0, 1, [{
  offset: 0,
  color: 'rgba(0, 255, 233,0)'
}, {
  offset: 0.5,
  color: 'rgba(255, 255, 255,1)',
}, {
  offset: 1,
  color: 'rgba(0, 255, 233,0)'
}], false)