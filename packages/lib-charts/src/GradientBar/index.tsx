import React from 'react'
import ReactEcharts from 'echarts-for-react'
import {AxisData, ChartOption, SeriesBarData, EChartOption} from '@/types'
import {shadowBarOption} from '@/utils'
import {AXIS_LABEL} from '@/styles/Colors'
import {BAR_COLOR, BAR_EMPHASIS_COLOR} from '@/styles/LinearGradient'

let zoomSize = 6
export interface GradientBarProps {
  yAxisMax?: number;
  xAxisData?: AxisData[];
  yAxisData?: SeriesBarData;
}

const GradientBar: React.FC<GradientBarProps> = props => {
  const {xAxisData = [], yAxisData = [], yAxisMax = 500} = props

  const option: ChartOption<EChartOption.SeriesBar> = {
    title: {
      text: '特性示例：渐变色 阴影 点击缩放',
      subtext: 'Feature Sample: Gradient Color, Shadow, Click Zoom',
    },
    xAxis: {
      data: xAxisData,
      axisLabel: {
        inside: true,
        color: '#fff',
      },
      axisTick: {
        show: false,
      },
      axisLine: {
        show: false,
      },
      z: 10,
    },
    yAxis: {
      axisLine: {
        show: false,
      },
      axisTick: {
        show: false,
      },
      axisLabel: {
        color: AXIS_LABEL,
      },
    },
    dataZoom: [
      {
        type: 'inside',
      },
    ],
    series: [
      shadowBarOption(
        new Array(yAxisData.length).fill(yAxisMax), 0,
      ),
      {
        type: 'bar',
        itemStyle: {
          color: BAR_COLOR,
          barBorderRadius: [4, 4, 0, 0]
        },
        emphasis: {
          itemStyle: {
            color: BAR_EMPHASIS_COLOR,
          },
        },
        data: yAxisData,
      },
    ],
  }

  return (
    <ReactEcharts
      option={option}
      style={{height: 600}}
      onEvents={{
        click: (params, e) => {
          const {dataIndex} = params
          const halfZoomSize = zoomSize / 2
          console.log(xAxisData[Math.max(dataIndex - halfZoomSize, 0)])
          e._api.dispatchAction({
            type: 'dataZoom',
            startValue: xAxisData[Math.max(dataIndex - halfZoomSize, 0)],
            endValue: xAxisData[Math.min(dataIndex + halfZoomSize, yAxisData.length - 1)],
          })
        },
      }}
    />
  )
}

export default GradientBar