import echarts from 'echarts'
import ReactEcharts from 'echarts-for-react'
import {ChartOption, EChartOption} from '@/types'
import {LINE_COLOR} from '@/styles/LinearGradient'
import React from 'react'

const areaStyleColor = new echarts.graphic.LinearGradient(0, 0, 0, 1, [{
  offset: 0,
  color: 'rgba(108,80,243,0.3)'
},
  {
    offset: 1,
    color: 'rgba(108,80,243,0)'
  }
], false);

const areaStyleColor2 = new echarts.graphic.LinearGradient(0, 0, 0, 1, [{
  offset: 0,
  color: 'rgba(0,202,149,0.3)'
},
  {
    offset: 1,
    color: 'rgba(0,202,149,0)'
  }
], false)


const option: ChartOption<EChartOption.SeriesLine> = {
  backgroundColor: '#080b30',
  title: {
    text: '渐变折线图',
    textStyle: {
      align: 'center',
      color: '#fff',
      fontSize: 20,
    },
    top: '5%',
    left: 'center',
  },
  tooltip: {
    trigger: 'axis',
    axisPointer: {
      lineStyle: {
        color: LINE_COLOR
      },
    },
  },
  grid: {
    top: '15%',
    left: '5%',
    right: '5%',
    bottom: '15%',
    // containLabel: true
  },
  xAxis: [{
    type: 'category',
    axisLine: {
      show: true
    },
    axisLabel: {
      color: '#fff'
    },
    splitLine: {
      show: false
    },
    boundaryGap: false,
    data: ['A', 'B', 'C', 'D', 'E', 'F'],
  }],

  yAxis: [{
    type: 'value',
    min: 0,
    splitNumber: 4,
    splitLine: {
      show: true,
      lineStyle: {
        color: 'rgba(255,255,255,0.1)'
      }
    },
    axisLine: {
      show: false,
    },
    axisLabel: {
      show: false,
      margin: 20,
      color: '#d1e6eb',
    },
    axisTick: {
      show: false,
    },
  }],
  series: [{
    name: '注册总量',
    type: 'line',
    showAllSymbol: true,
    symbol: 'circle',
    symbolSize: 25,
    // lineStyle: {
    //   normal: {
    //     color: "#6c50f3",
    //     shadowColor: 'rgba(0, 0, 0, .3)',
    //     shadowBlur: 0,
    //     shadowOffsetY: 5,
    //     shadowOffsetX: 5,
    //   },
    // } as any,
    label: {
      show: true,
      position: 'top',
      color: '#6c50f3',
    },
    itemStyle: {
      color: "#6c50f3",
      borderColor: "#fff",
      borderWidth: 3,
      shadowColor: 'rgba(0, 0, 0, .3)',
      shadowBlur: 0,
      shadowOffsetY: 2,
      shadowOffsetX: 2,
    },
    areaStyle: {
      color: areaStyleColor,
      shadowColor: 'rgba(108,80,243, 0.9)',
      shadowBlur: 20
    },
    data: [502.84, 205.97, 332.79, 281.55, 398.35, 214.02, ]
  },
    {
      name: '注册总量',
      type: 'line',
      // smooth: true, //是否平滑
      showAllSymbol: true,
      // symbol: 'image://./static/images/guang-circle.png',
      symbol: 'circle',
      symbolSize: 25,
      // lineStyle: {
      //   normal: {
      //     color: "#00ca95",
      //     shadowColor: 'rgba(0, 0, 0, .3)',
      //     shadowBlur: 0,
      //     shadowOffsetY: 5,
      //     shadowOffsetX: 5,
      //   },
      // } as any,
      label: {
        show: true,
        position: 'top',
        color: '#00ca95',
      },

      itemStyle: {
        color: "#00ca95",
        borderColor: "#fff",
        borderWidth: 3,
        shadowColor: 'rgba(0, 0, 0, .3)',
        shadowBlur: 0,
        shadowOffsetY: 2,
        shadowOffsetX: 2,
      },
      areaStyle: {
        color: areaStyleColor2 as any,
        shadowColor: 'rgba(0,202,149, 0.9)',
        shadowBlur: 20
      },
      data: [281.55, 398.35, 214.02, 179.55, 289.57, 356.14, ],
    },
  ]
}

const GradientLine: React.FC = () => {
  return (
    <ReactEcharts
      option={option}
    />
  )
}

export default GradientLine