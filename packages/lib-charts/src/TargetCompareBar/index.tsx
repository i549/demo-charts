import React from 'react'
import ReactEcharts from 'echarts-for-react'
import {ChartOption} from '@/types'
import {transparentBarOption} from '@/utils'

const targets: number[] = [2500, 1000, 500, 180, 920, 280]
const split: number = 10
const data: number[] = [2900, 1200, 300, 200, 900, 300]
const defaultOptions: ChartOption = {
  title: {
    text: '深圳月最低生活费组成（单位:元）',
    subtext: 'From ExcelHome',
    sublink: 'http://e.weibo.com/1341556070/AjQH99che',
  },
  tooltip: {
    trigger: 'axis',
    axisPointer: {           // 坐标轴指示器，坐标轴触发有效
      type: 'shadow',        // 默认为直线，可选为：'line' | 'shadow'
    },
    formatter: (params) => {
      const tar = Array.isArray(params) ? params[0] : params
      const value = data[tar.dataIndex || 0]
      const target = targets[tar.dataIndex || 0]
      return (
        tar.name + ' : ' + value + '<br/>' +
        '目标值 : ' + target + '<br/>' +
        (value >= target ? '超出目标 : ' + (value - target) : '距离目标 : ' + (target - value))
      )
    },
  },
  grid: {
    left: '3%',
    right: '4%',
    bottom: '3%',
    containLabel: true,
  },
  xAxis: {
    type: 'category',
    splitLine: {show: false},
    data: ['总费用', '房租', '水电费', '交通费', '伙食费', '日用品数'],
  },
  yAxis: {
    type: 'value',
  },
  series: [
    {
      name: '低于目标部分值',
      type: 'bar',
      stack: '目标',
      barMaxWidth: 50,
      itemStyle: {
        barBorderColor: '#339eef',
        color: '#339eef',
      },
      data: data.map((d, i) => d > targets[i] ? targets[i] : d),
    },
    transparentBarOption({
      name: '低于目标部分辅助',
      stack: '目标',
      data: data.map((d, i) => d > targets[i] ? 0 : targets[i] - d),
    }),
    {
      name: '目标值',
      type: 'bar',
      stack: '目标',
      barMaxWidth: 50,
      itemStyle: {
        barBorderColor: '#f30a03',
        color: '#f30a03',
      },
      data: new Array(data.length).fill(split),
    },
    {
      name: '高于目标部分值',
      type: 'bar',
      stack: '目标',
      barMaxWidth: 50,
      itemStyle: {
        barBorderColor: '#339eef',
        color: '#339eef',
      },
      data: data.map((d, i) => d > (targets[i] + split) ? d - targets[i] - split : null),
    },
  ],
}

const TargetCompareBar: React.FC = props => {
  return (
    <ReactEcharts
      option={defaultOptions}
      style={{height: 600}}
    />
  )
}

export default TargetCompareBar